package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import javax.tools.JavaCompiler;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Set;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Timer timer;

    private java.util.List<Balloon> balloon;
    private JButton moveButton;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {

        this.balloon = new ArrayList(15);

        for (int i = 0; i < 15; i++) {
            balloon.add(new Balloon((int) (Math.random() * 300), (int) (Math.random() * 600)));

        }

        setBackground(Color.white);

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);
        addKeyListener(this);

        this.timer = new Timer(200, this);
        this.timer.start();

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == timer) {
            for (Balloon b : balloon) {
                b.move();
                requestFocusInWindow();
                repaint();
            }
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.



    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Balloon b : balloon) {
            b.draw(g);
        }


        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
// Use this one
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            for (Balloon b : balloon) { b.setDirection(Direction.Up); }
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            for (Balloon b : balloon) { b.setDirection(Direction.Down); }
        }

        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            for (Balloon b : balloon) { b.setDirection(Direction.Left); }
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            for (Balloon b : balloon) { b.setDirection(Direction.Right); }
        }

        if (e.getKeyCode() == KeyEvent.VK_S) {
            for (Balloon b : balloon) { b.setDirection(Direction.None); }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
