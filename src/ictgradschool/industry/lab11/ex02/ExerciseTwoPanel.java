package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 * <p>
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    JTextField firstNum;
    JTextField secondNum;
    JButton addNum;
    JButton subtractNum;
    JTextField result;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        FlowLayout layout = new FlowLayout();
        setLayout(layout);


        firstNum = new JTextField(10);
        secondNum = new JTextField(10);
        addNum = new JButton("Add");
        subtractNum = new JButton("Subtract");
        result = new JTextField(20);


        JLabel resultLabel = new JLabel("Result: ");


        this.add(firstNum);
        this.add(secondNum);
        this.add(addNum);
        this.add(subtractNum);
        this.add(resultLabel);
        this.add(result);


        addNum.addActionListener(this);
        subtractNum.addActionListener(this);

    }

    public void actionPerformed(ActionEvent event) {

        if (event.getSource() == addNum) {
            double leftNum = Double.parseDouble(firstNum.getText());
            double rightNum = Double.parseDouble(secondNum.getText());
            result.setText(String.valueOf(leftNum + rightNum));

        } else if (event.getSource() == subtractNum) {
            double leftNum = Double.parseDouble(firstNum.getText());
            double rightNum = Double.parseDouble(secondNum.getText());
            result.setText(String.valueOf(leftNum - rightNum));
        }

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     *
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
