package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.

    JTextField heightInMetres;
    JTextField weightInKgs;
    JButton calculateBMI;
    JTextField yourBMI;
    JButton calculateHealthyWeight;
    JTextField yourHealthyWeight;

    private JButton calculateBMIButton;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);
        FlowLayout layout = new FlowLayout();

        setLayout(layout);
        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        heightInMetres = new JTextField(10);
        weightInKgs = new JTextField(10);
        calculateBMI = new JButton("Calculate BMI");
        yourBMI = new JTextField(10);
        calculateHealthyWeight = new JButton("Calculate Healthy Weight");
        yourHealthyWeight = new JTextField(10);


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel heightLabel = new JLabel("Height in metres: ");
        this.add(heightLabel);
        this.add(heightInMetres);

        JLabel weightLabel = new JLabel("Weight in kilograms: ");
        this.add(weightLabel);
        this.add(weightInKgs);

        this.add(calculateBMI);
        JLabel bmiLabel = new JLabel("Your Body Mass Index (BMI) is: ");
        this.add(bmiLabel);
        this.add(yourBMI);

        this.add(calculateHealthyWeight);
        JLabel yourHealthyWeightLabel = new JLabel("Maximum Healthy Weight for your Height: ");
        this.add(yourHealthyWeightLabel);
        this.add(yourHealthyWeight);

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)


        // TODO Add Action Listeners for the JButtons

        calculateBMI.addActionListener(this);
        calculateHealthyWeight.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        if (event.getSource() == calculateBMI) {
            double height = Double.parseDouble(heightInMetres.getText());
            double weight = Double.parseDouble(weightInKgs.getText());
            yourBMI.setText(String.valueOf(weight / (height * height )));

        } else if (event.getSource() == calculateHealthyWeight) {
            double height = Double.parseDouble(heightInMetres.getText());
            yourHealthyWeight.setText(String.valueOf(24.9 * height * height));
        }
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     *
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
