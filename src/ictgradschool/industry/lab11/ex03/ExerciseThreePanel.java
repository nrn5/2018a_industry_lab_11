package ictgradschool.industry.lab11.ex03;

import sun.java2d.loops.DrawLine;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.


        // Chimney
        Color fillColor = CHIMNEY_COLOR;
        Color borderColor = OUTLINE_COLOR;
        int x = left + 7 * size;
        int y = top + 1 * size;
        int width = 1 * size;
        int height = 2 * size;

        g.setColor(fillColor);
        g.fillRect(x, y, width, height);
        g.setColor(borderColor);
        g.drawRect(x, y, width, height);


        // Roof
        fillColor = ROOF_COLOR;
        borderColor = OUTLINE_COLOR;
        int[] polyX = new int[]{
                left + 0,
                left + 10 * size,
                left + 5 * size,
                left + 0};
        int[] polyY = new int[]{
                top + 5 * size,
                top + 5 * size,
                top + 0,
                top + 5 * size};
        g.setColor(fillColor);
        g.fillPolygon(polyX, polyY, polyX.length);
        g.setColor(borderColor);
        g.drawPolygon(polyX, polyY, polyX.length);


        //  Walls
        fillColor = MAIN_COLOR;
        borderColor = OUTLINE_COLOR;
        x = left + 0 * size;
        y = top + 5 * size;
        width = 10 * size;
        height = 7 * size;

        g.setColor(fillColor);
        g.fillRect(x, y, width, height);
        g.setColor(borderColor);
        g.drawRect(x, y, width, height);


        //  Door
        fillColor = DOOR_COLOR;
        borderColor = OUTLINE_COLOR;
        x = left + 4 * size;
        y = top + 8 * size;
        width = 2 * size;
        height = 4 * size;

        g.setColor(fillColor);
        g.fillRect(x, y, width, height);
        g.setColor(borderColor);
        g.drawRect(x, y, width, height);


        //  Window1
        fillColor = WINDOW_COLOR;
        borderColor = OUTLINE_COLOR;
        x = left + 1 * size;
        y = top + 7 * size;
        width = 2 * size;
        height = 2 * size;

        g.setColor(fillColor);
        g.fillRect(x, y, width, height);
        g.setColor(borderColor);
        g.drawRect(x, y, width, height);


        //  Window2
        fillColor = WINDOW_COLOR;
        borderColor = OUTLINE_COLOR;
        x = left + 7 * size;
        y = top + 7 * size;
        width = 2 * size;
        height = 2 * size;

        g.setColor(fillColor);
        g.fillRect(x, y, width, height);
        g.setColor(borderColor);
        g.drawRect(x, y, width, height);


        //  Horizontal Line1
        borderColor = OUTLINE_COLOR;
        g.setColor(borderColor);
        g.drawLine(left + 1 * size, top + 8 * size, left + 3 * size , top + 8 * size);


        //  Vertical Line1
        borderColor = OUTLINE_COLOR;
        g.setColor(borderColor);
        g.drawLine(left + 2 * size, top + 7 * size, left + 2 * size , top + 9 * size);


        //  Horizontal Line2
        borderColor = OUTLINE_COLOR;
        g.setColor(borderColor);
        g.drawLine(left + 7 * size, top + 8 * size, left + 9 * size , top + 8 * size);


        //  Vertical Line2
        borderColor = OUTLINE_COLOR;
        g.setColor(borderColor);
        g.drawLine(left + 8 * size, top + 7 * size, left + 8 * size , top + 9 * size);

    }
}
